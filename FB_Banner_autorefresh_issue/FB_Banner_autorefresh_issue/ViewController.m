//
//  ViewController.m
//  FB_Banner_autorefresh_issue
//
//  Created by Alex Nadein on 7/19/17.
//  Copyright © 2017 Alex Nadein. All rights reserved.
//

#import "ViewController.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>

@interface ViewController ()<FBAdViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property(nonatomic, strong) FBAdView *bannerView;
@property(nonatomic, assign) NSUInteger counter;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setCounter:0];
    [self setupBannerAd]; // banner requested once
}

- (void)setupBannerAd
{
    BOOL isIPAD = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    FBAdSize adSize = isIPAD ? kFBAdSizeHeight90Banner : kFBAdSizeHeight50Banner;
    
    FBAdView *adView = [[FBAdView alloc] initWithPlacementID:@"1505371012847817_1505371706181081"
                                                      adSize:adSize
                                          rootViewController:self];
    
    [adView setDelegate:self];
    [adView disableAutoRefresh]; // auto-refresh should be disabled for this instance
    [adView loadAd];
    [self setBannerView:adView];
}

- (void)incrementCounter
{
    _counter = _counter + 1;
    [_counterLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)_counter]];
}

- (void)showBanner:(UIView *)banner
{
    if (banner != nil)
    {
        [[self view] addSubview:banner];
        
        [banner setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:banner
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:[self view]
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1.f constant:0.f];
        
        NSLayoutConstraint *horizontalCenter = [NSLayoutConstraint constraintWithItem:banner
                                                                            attribute:NSLayoutAttributeCenterX
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:[self view]
                                                                            attribute:NSLayoutAttributeCenterX
                                                                           multiplier:1.f constant:0.f];
        
        [[self view] addConstraints:@[bottom, horizontalCenter]];
        
        NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:banner
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.f constant:CGRectGetWidth([banner frame])];
        
        NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:banner
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.f constant:CGRectGetHeight([banner frame])];
        
        [banner addConstraints:@[width, height]];
    }
}

#pragma mark - FBAdViewDelegate

- (void)adViewDidClick:(FBAdView *)adView
{
    // skip
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    // skip
}

- (void)adViewDidLoad:(FBAdView *)adView
{
    // this method will be called on auto-refresh success
    [self incrementCounter];
    [self showBanner:adView];
}

- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error
{
    // this method will be called on auto-refresh failure
    [self incrementCounter];
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    // skip
}

- (UIViewController *)viewControllerForPresentingModalView
{
    return self;
}

@end
